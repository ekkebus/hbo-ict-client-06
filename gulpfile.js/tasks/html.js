const {src, dest} = require("gulp");

const fn = function (sourceFiles,distributionPath) {
    //het resultaat van src wordt opgeslagen in een constante
    const htmlFiles = src(sourceFiles);
    //je returned dus een function, met daarin een Promise
    return function () {
        return htmlFiles
            .pipe(dest(distributionPath));
    }
};

exports.html = fn;