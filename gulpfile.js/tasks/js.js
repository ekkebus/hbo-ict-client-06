const { src, dest } = require('gulp');
const concat = require('gulp-concat');
var order = require('gulp-order');

const fn = function (sourceFiles, distFile, distributionPath) {
    //je returned dus een function, met daarin een Promise
    return function () {
        return src(sourceFiles)
            .pipe(order(['**/jquery*.js','**/Chart*.js']))
            .pipe(concat(distFile))
            .pipe(dest(distributionPath));
    }
};

exports.js = fn;