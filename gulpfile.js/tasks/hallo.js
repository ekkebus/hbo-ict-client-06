const fn = function (voornaam) {
    //je returned dus een function, met daarin een Promise
    return function () {
        console.log(`Hallo! De taak is uitgevoerd, ${voornaam}!`);
        return Promise.resolve('Klaar');
    }
};
exports.hello = fn;