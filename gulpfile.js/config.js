exports.config = {
    name: 'Swen-Peter',
    distPath: './dist',
    srcFiles: {
        js: ['./src/**/*.js','./vendor/*.js',],
        html: ['./src/*.html'],
    },
    distFiles: {
        js: 'app.js',
    }
};