//gulp imports
const { parallel } = require("gulp");

//imports uit de gulpfile.js foler
const config = require('./config').config;
const hallo = require('./tasks/hallo').hello(config.name);
hallo.displayName = 'Hallo';

const html = require('./tasks/html').html(config.srcFiles.html, config.distPath);
html.displayName = 'HTML';

const js = require('./tasks/js').js(config.srcFiles.js, config.distFiles.js, config.distPath);
js.displayName = 'JS';

//exporteer de build
exports.build = parallel(hallo, html, js);