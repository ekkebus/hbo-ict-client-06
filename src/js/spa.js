const SPA = (function () {
    var _configMap = {
        chartContextType: '2d',
        charType: 'pie',
        chartOptions: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Chart demo'
            }
        }
    };
    var _stateMap = {
        chartArea: null,
        chart: null,
        chartData: {
            labels: [],
            datasets: [{
                backgroundColor:['#00ff00','#ff00ff'],
                data: []
            }]
        }
    }

    function _init(chartArea) {
        _stateMap.chartArea = document.getElementById(chartArea).getContext(_configMap.chartContextType);
        _initDiagram();
        return true;
    }

    function _initDiagram() {
        //initialiseer de nieuwe chart
        _stateMap.chart = new Chart(_stateMap.chartArea, {
            type: _configMap.charType,
            data: _stateMap.chartData,
            options: _configMap.chartOptions
        });
        _drawInitialData();
    }

    function _drawInitialData() {
        //voeg een nieuw label toe
        _stateMap.chartData.labels.push('One');
        _stateMap.chartData.labels.push('Two');

        //push de nieuwe data toe aan de dataset
        _stateMap.chartData.datasets[0].data.push(50);
        _stateMap.chartData.datasets[0].data.push(50);
        
        //update de chart met de nieuwe dataset
        _stateMap.chart.update();
    }

    function _updateValue(datasetIndex,dataIndex,newValue){
        _stateMap.chartData.datasets[datasetIndex].data[dataIndex]= newValue;
        //update de chart met de nieuwe dataset
        _stateMap.chart.update();
    }

    return {
        init: _init,
        updateValue:_updateValue
    }
})();